# Create a Class Person

Creating a Person class, using overloads to input values, linking different files, and creating/using methods throughout. 


## About

Testing out and learning the basics of methods, imports, classes and overloads. An assignment.
 
Creating class attributes through methods.   
Linking different files and their respective methods.    
Displaying data through the communication between classes.

*See comments within the code to see the respective functions*.


