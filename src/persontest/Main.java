package persontest;

import persontest.Person.Person;

public class Main {
    public static void main(String[] args) {
        Person obiwan = new Person(
                "Obi-wan Kenobi",
                182,
                "Male",
                "Utapau");

        Person grevious = new Person(
                "General Grevious",
                216,
                "Cyborg",
                "Utapau");


        //Prints the obiwan Person through introducePerson method, along with a greeting!
        System.out.println(obiwan.introducePerson());

        //Prints the overloaded introducePerson method, if true, a reply is made!
        System.out.println(grevious.introducePerson(grevious.getPersonHomeTown()));


        //Prints Person: "Obiwan" height value as inches.
        System.out.println(obiwan.getHeightInches() + " inches!");

    }
}
