package persontest.Person;


//Class: contains the individual's name, height, gender and hometown.
public class Person {
    private String personName;
    private int personHeight;
    private String personGender;
    private String personHomeTown;

    //Construct: Adds values to the Person class.
    public Person(String personName, int personHeight, String personGender, String personHomeTown) {
        this.personName = personName;
        this.personHeight = personHeight;
        this.personGender = personGender;
        this.personHomeTown = personHomeTown;
    }

    //Method: Returns the Person height, in inches.
    public int getHeightInches() {
        double inch = 0.393;
        return (int) (personHeight * inch);
    }

    //Method: Prints a greeting from a Person, followed by their name, their hometown.
    public String introducePerson() {
        return "Hello there! -" + personName + ", " + personHomeTown;
    }

    //Method: Reads the value of hometown.
    public String getPersonHomeTown() {
        return personHomeTown;
    }

    //Method: Checks if the hometown is the same, if it is, the greeting is answered!
    public String introducePerson(String sameHometown) {
        if (personHomeTown.equals(sameHometown)) {
            return "GENERAL KENOBI! " + personName + ", " + personHomeTown;
        } else {
            return introducePerson();
        }


    }

}

